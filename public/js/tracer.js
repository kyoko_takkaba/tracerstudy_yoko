var state ={
    "identitas_1_1":{
      "value":"",
      "validation":"alpha",
      "enable":true,
      "show":true,
    },
    "identitas_1_2":{
      "value":"",
      "validation":"numeric=9",
      "enable":true,
      "show":true,
    },
    "identitas_1_3":{
      "value":"",
      "validation":"email",
      "enable":true,
      "show":true,
    },
    "identitas_1_4":{
      "value":"",
      "validation":"numeric",
      "enable":true,
      "show":true,
    },
    "pekerjaan_1":{
      "value":"",
      "enable":true,
      "show":true,
      "subitem":["pekerjaan_1_1","pekerjaan_1_2"]
    },
    "pekerjaan_1_1":{
      "value":"",
      "validation":"numeric",
      "enable":false,
      "show":true,
      "enable_if":{
        "pekerjaan_1":"a"
      }
    },
    "pekerjaan_1_2":{
      "value":"",
      "validation":"numeric",
      "enable":false,
      "show":true,
      "enable_if":{
        "pekerjaan_1":"b"
      }
    },
    "pekerjaan_2":{
      "value":"",
      "enable":false,
      "show":false,
      "enable_if":{
        "pekerjaan_1":"c"
      },
      "show_if":{
        "pekerjaan_1":"c"
      },
      "subitem":["pekerjaan_2OT"]
    },
    "pekerjaan_2OT":{
      "value":"",
      "validation":"numeric",
      "enable":false,
      "show":true,
      "enable_if":{
        "pekerjaan_2":"e"
      }
    },
    "pekerjaan_3":{
      "value":"",
      "enable":false,
      "show":false,
      "subitem":["pekerjaan_3OT"],
      "enable_if":{
        "or":{
          "condition1":{
            "pekerjaan_1":"a"
          },
          "condition2":{
            "pekerjaan_1":"b"
          }
        }
      },
      "show_if":{
        "or":{
          "condition1":{
            "pekerjaan_1":"a"
          },
          "condition2":{
            "pekerjaan_1":"b"
          }
        }
      }
    },
    "pekerjaan_3OT":{
      "value":"",
      "enable":false,
      "show":true,
      "enable_if":{
        "has":{
          "pekerjaan_3":"o"
        }
      }
    },
    "pekerjaan_4":{
      "value":"",
      "enable":true,
      "show":true
    },
    "pekerjaan_5":{
      "value":"temp",
      "enable":false,
      "show":false,
      "enable_if":{
            "pekerjaan_4":"a"
      },
      "show_if":{
            "pekerjaan_4":"a"
      },
      "subitem":["pekerjaan_5_1","pekerjaan_5_2","pekerjaan_5_3","pekerjaan_5_4"]
  },
  "pekerjaan_5_1":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_5_2":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_5_3":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_5_4":{
    "value":"",
    "enable":false,
    "show":true
  },
  "pekerjaan_6":{
    "value":"",
    "enable":false,
    "show":false,
    "subitem":["pekerjaan_6OT"],
    "enable_if":{
      "pekerjaan_4":"b"
    },
    "show_if":{
      "pekerjaan_4":"b"
    }
  },
  "pekerjaan_6OT":{
    "value":"",
    "enable":false,
    "show":false,
    "enable_if":{
      "pekerjaan_6":"e"
    }
  },
  "pekerjaan_7":{
    "value":"temp",
    "enable":false,
    "show":false,
    "enable_if":{
      "pekerjaan_4":"b"
    },
    "show_if":{
      "pekerjaan_4":"b"
    },
    "subitem":["pekerjaan_7_1_1","pekerjaan_7_1_2","pekerjaan_7_2_1","pekerjaan_7_2_2","pekerjaan_7_3_1","pekerjaan_7_3_2",]
  },
  "pekerjaan_7_1_1":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_7_1_2":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_7_2_1":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_7_2_2":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_7_3_1":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_7_3_2":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_8":{
    "value":"",
    "enable":false,
    "show":false,
    "enable_if":{
      "pekerjaan_4":"a"
    },
    "show_if":{
      "pekerjaan_4":"a"
    },
  },
  "pekerjaan_9":{
    "value":"",
    "enable":true,
    "show":true
  },
  "pekerjaan_10":{
    "value":"",
    "enable":false,
    "show":false,
    "enable_if":{
      "pekerjaan_9":"a"
    },
    "show_if":{
      "pekerjaan_9":"a"
    },
  },
  "pekerjaan_10_1":{
    "value":"",
    "enable":false,
    "show":true,
  },
  "pekerjaan_10_2":{
    "value":"",
    "enable":false,
    "show":false,
  },
  "pekerjaan_10_3":{
    "value":"",
    "enable":false,
    "show":false,
  }
};

function enableHandler(key){
  if(state[key]["enable"]){
    $("div.question"+"#"+key).removeClass("disabled");
    $("input"+"[name="+key+"]").prop("disabled",false);
  }else{
    $("div.question"+"#"+key).addClass("disabled");
    $("input"+"[name="+key+"]").prop("disabled",true);
    disable(key);
    if(state[key]["subitem"]!= undefined){
      $.each(state[key]["subitem"],function(key,value){
        disable(value);
      });
    }
    if($("input"+"[name="+key+"]").length){
      state[key]["value"] = "";      
    }
  }
}

function disable(key){
  switch($("input"+"[name="+key+"]").attr("type")){
    case "text":
      $("input"+"[name="+key+"]").prop("value","");
    break;
    case "radio":
      $("input"+"[name="+key+"]").prop("checked",false);
    break;
    case "checkbox":
      $("input"+"[name="+key+"]").prop("checked",false);
    break;
  }
}

function showHandler(key){
  if(state[key]["show"]){
    $("div.question"+"#"+key).removeClass("hidden");
    $("input"+"[name="+key+"]").removeClass("hidden");
  }else{
    $("div.question"+"#"+key).addClass("hidden");
    $("input"+"[name="+key+"]").addClass("hidden");
  }
}

function showError(error){
  $.each(error, function(key,value){
    if($("div.question#"+value).length){
      $("div.question#"+value).addClass("error");
    }else{
      $("input[name="+value+"]").addClass("error");
    }
  })
}

function refresh(){
  var temp;
  $.each(state,function(key,value){
    if(state[key]["enable_if"]!= undefined){
      temp = checkCondition(state[key]["enable_if"]);
      state[key]["enable"] = temp;
      enableHandler(key);
    }
    if(state[key]["show_if"]!= undefined){
      temp = checkCondition(state[key]["show_if"]);
      state[key]["show"] = temp;
      showHandler(key);
    }
  });
}

function checkCondition(conditionArray){
  var temp = false;
  $.each(conditionArray,function(key,value){
      if (key ==  "or") {
        temp = or(value);
      }else if (key == "and") {
        temp = and(value);
      }else if(key =="has"){
        temp = has(value);
      }else{
        temp = is(key,value);
      }
  });
  return temp;
}

function is(question, value){
  if(value == state[question]["value"]){
      return true;
  }
  return false;
}

function has(conditionArray){
  var temp = false;
  $.each(conditionArray, function(key,value){
    if (state[key]["value"].indexOf(value) != -1){
      temp =  true;
    }
  });
  return temp;
}

function or(conditionArray){
  var temp =false;
  var condition1;
  var condition2;
//  var stringtemp="";
  $.each(conditionArray, function(key,condition){
    temp = checkCondition(condition);
    if (key == "condition1"){
      condition1 = temp;
    }else if (key=="condition2") {
      condition2 = temp;
    }
  });
    return (condition1 || condition2);
}

function and(conditionArray){
  var temp =false;
  var condition1;
  var condition2;
//  var stringtemp="";
  $.each(conditionArray, function(key,condition){
    temp = checkCondition(condition);
    if (key == "condition1"){
      condition1 = temp;
    }else if (key=="condition2") {
      condition2 = temp;
    }
  });
    return (condition1 && condition2);
}

function receiveQuestionnaireJSON(json){
  questionnaire = json;
}

function initState(){
    $("input[type=radio]:checked").each(function(){
      if($(this).prop("value") != undefined){
        state[$(this).prop("name")]["value"] = $(this).prop("value");
      }
    });
    $("input[type=text]").each(function(){
      if($(this).prop("value") != undefined){
        state[$(this).prop("name")]["value"] = $(this).prop("value");
      }
    });
    $("input[type=checkbox]:checked").each(function(){
      if($(this).prop("value") != undefined){
        if (state[$(this).prop("name")]["value"] ==""){
          state[$(this).prop("name")]["value"] = $(this).prop("value");
        }else{
          state[$(this).prop("name")]["value"] += "," + $(this).prop("value");
        }
      }
    });
}

function validateInput(value,style){
  var temp = true;

  if(value!="" && value!= undefined && value!=null){
    if (style!=undefined){
      style = style.split("=");
      switch(style[0]){
        case "email":
          if(!/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value)){
            temp =  false;
          }
        break;
        case "alpha":
          if(!value.match('^[a-zA-Z ]*$')){
            temp = false;
          }
        break;
        case "numeric":
          if(!value.match('^[0-9]*$')){
            temp =  false;
          }
          if(style[1] != undefined){
            if(value.length != parseInt(style[1])){
              temp = false;
            }
          }
        break;
        case "alphanumeric":
        if(!value.match('^[a-zA-Z0-9 ]*$')){
          temp = false;
        }
        break;
        default:
          temp = true;
        break;
      }
    }
  }else{
    temp = false;
  }
  return temp;
}

$(document).ready(function(){
  initState();
  refresh();
/*  $('div.answerItem').on("click",function(){
    var tempInput = $(this).children("input");
    switch(tempInput.attr("type")){
      case "radio":
        tempInput.prop("checked",true);
        tempInput.trigger("change");
        break;
      case "checkbox":
        alert("aaaaaaaaaaaaaaa");
        tempInput.prop("checked", !tempInput.prop("checked"));
//        alert(tempInput.attr("checked"));
//        tempInput.trigger("change");
        break;
    }
  });*/
  $("input").on("click", function(){
    $(this).trigger("change");
  });

  $("form").on("submit",function(e){
      e.preventDefault();
      var data={};
      var error=[];
      var valid = true;
      $(".error").each(function(){
        $(this).removeClass("error");
      });
      $.each(state,function(key,content){
        if(content["enable"]){
          if(validateInput(content["value"],content["validation"])){
            data[key] = content["value"];
          }else{
            error.push(key);
            valid =false;
          }
        }
      });

      if (valid){
        if($(this).attr("method") == "post"){
          $.post($(this).attr("action"),data,function(data){
            $("body").html(data);
          });
        }
      }else{
        showError(error);
      }
  });

  $(document).on("change",'input', function(){
      switch($(this).prop("type")){
          case "radio":
            if($(this).prop("checked")){
              state[$(this).prop("name")]["value"] = $(this).prop("value");
            }
          break;
          case "checkbox":
            if($(this).prop("checked")){
              if(state[$(this).prop("name")]["value"].indexOf($(this).prop("value")) == -1){
                if(state[$(this).prop("name")]["value"]!=""){
                  state[$(this).prop("name")]["value"]+=",";
                }
                state[$(this).prop("name")]["value"]+=$(this).prop("value");
              }
            }else{
              state[$(this).prop("name")]["value"] = state[$(this).prop("name")]["value"].replace($(this).prop("value"),"");
              state[$(this).prop("name")]["value"] = state[$(this).prop("name")]["value"].replace(",,", ",");
              if(state[$(this).prop("name")]["value"].charAt(0) == ","){
                state[$(this).prop("name")]["value"] = state[$(this).prop("name")]["value"].substr(1);
              }
              if(state[$(this).prop("name")]["value"].charAt(state[$(this).prop("name")]["value"].length-1) == ","){
                state[$(this).prop("name")]["value"] = state[$(this).prop("name")]["value"].substr(0,state[$(this).prop("name")]["value"].length-1);
              }
//              console.log($(this).prop("value"));
            }

//           console.log(state[$(this).prop("name")]["value"])
          break;
          case "text":
            state[$(this).prop("name")]["value"] = $(this).prop("value");
          break;
      }
      refresh();
  });
});

/*
questionnaire = {
  "1":{
    "caption":"Pekerjaan",
    "questions":{
      "1":{
        "caption":"Apa alasan utama anda TIDAK mencari pekerjaan setelah lulus kuliah?",
        "enable":false,
        "show":false,
        "variable":"pekerjaan-1",
        "enable_if":{
            "and":{
              "pekerjaan-1":1,
              "pekerjaan-2":2
            }
        },
        "show_if":{
            "and":{
              "pekerjaan-1":1,
              "pekerjaan-2":2
            }
        },
        "answer":{
            "radio":{
              "value":1,
              "label":"Ya"
            },
            "radio":{
              "value":2,
              "label":"Tidak"
            },
            "radio":{
              "value":3,
              "label": "Lainnya",
              "text":{
                "variable":"pekerjaan1-OT"
                "enable":false,
                "show":false,
                "enable_if":{
                  "pekerjaan-1":3
                },
                "show_if":{
                  "pekerjaan-1":3
                }
              },
              "label": "jadi gitu"
            }
        }
      }
    }
  }
}
*/
