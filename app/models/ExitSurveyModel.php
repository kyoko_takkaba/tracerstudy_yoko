<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class ExitSurveyModel extends Eloquent {
	protected $fillable = array('id_alumni');
	protected $table    = 'exit_survey';	
	public $timestamps  = false;
}